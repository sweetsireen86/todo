let input = document.querySelector('.container__input');
let addButton = document.querySelector('.container__btn-add');

let list = document.querySelector('.container__list');

let removeButtons = document.querySelectorAll('.container__list-item__btn-remove');


function initTodoList() {
	let todoList = [];
	if (localStorage.getItem('todoList')) { // Проверяем что не пустое хранилище, затем - копируем содержимое, а если пустое, то не копируем - то оставляем пустой массив
		todoList = JSON.parse(localStorage.getItem('todoList'));
	}
	
	for (let i = 0; i < todoList.length; i++) {
		createTodoItem(todoList[i].task, todoList[i].isDone);
	}
}

function addTodoItem() {
	if (input.value === '')
		return;

	createTodoItem(input.value);
	addToLocaleStorage(input.value);

	input.value = '';
}

function createTodoItem(todoValue, isDone) {
	let todo = document.createElement('li');
	let span = document.createElement('span');
	let removeButton = document.createElement('button');
	let checkbox = document.createElement('input');

	todo.setAttribute('class', 'container__list-item');
	checkbox.setAttribute('type', 'checkbox');
	span.setAttribute('class', 'container__list-item__name');
	checkbox.setAttribute('class', 'container__list-item__checkbox');
	removeButton.setAttribute('class', 'container__list-item__btn-remove');

	checkbox.checked = isDone;
	span.innerHTML = todoValue;
	removeButton.innerHTML = 'Удалить';

	removeButton.addEventListener('click', () => removeTodoItem(todo, span.innerHTML));
	checkbox.addEventListener('change', () => toggleTodoIsDone(span.innerHTML));

	todo.appendChild(checkbox);
	todo.appendChild(span);
	todo.appendChild(removeButton);

	list.appendChild(todo);
}

function addToLocaleStorage(todoItem) {
	let todoList = [];
	if (localStorage.getItem('todoList')) {
		todoList = JSON.parse(localStorage.getItem('todoList'));
	}
	todoList.push({
		task: todoItem, 
		isDone: false,
	});

	localStorage.setItem('todoList', JSON.stringify(todoList));
}

function removeTodoItem(todoItem, todoValue) {
	let todoList = JSON.parse(localStorage.getItem('todoList'));
	let searchingTodoIndex = todoList.findIndex(loopTodo => loopTodo.task === todoValue);
	todoList.splice(searchingTodoIndex, 1);

	list.removeChild(todoItem);
	
	localStorage.setItem('todoList', JSON.stringify(todoList));
}

function toggleTodoIsDone(todoItem) {
	let todoList = JSON.parse(localStorage.getItem('todoList'));
	let searchingTodo = todoList.find(loopTodo => loopTodo.task === todoItem);
	
	searchingTodo.isDone = !searchingTodo.isDone;
	todoList = todoList.filter(loopTodo => loopTodo.task !== searchingTodo.task);

	todoList.push(searchingTodo);

	localStorage.setItem('todoList', JSON.stringify(todoList));

	// console.log(JSON.parse(localStorage.getItem('todoList')));
}

addButton.addEventListener('click', addTodoItem);

initTodoList();


